# Mobile Web - A Presentation

Install yarn:

https://yarnpkg.com/lang/en/docs/install/

## Project setup

```
yarn install
```

### Run the presentation

```
yarn run serve
```

Open browser to http://localhost:8081 and profit. Use the arrow keys to navigate, or press ESC to page through the slides.

(Note that the mobile responsive section will have a broken link because it requires a website that ran locally on my machine during the time of the presentation)
