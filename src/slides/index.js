import MediaQueries from "./MediaQueries.vue";
import Title from "./Title.vue";
import AboutMe from "./AboutMe.vue";
import MobileUsage from "./MobileUsage.vue";
import HowToMobile from "./HowToMobile.vue";
import MobileFirst from "./MobileFirst.vue";
import MobileResponsive from "./MobileResponsive.vue";
import ReachingMobile from "./ReachingMobile.vue";
import Techniques from "./Techniques.vue";
import WhichApproach from "./WhichApproach.vue";
import Flexbox from "./Flexbox.vue";
import Grid from "./Grid.vue";
import HelpfulThings from "./HelpfulThings.vue";

export default {
  Title,
  AboutMe,
  MobileUsage,
  ReachingMobile,
  HowToMobile,
  MobileResponsive,
  MobileFirst,
  WhichApproach,
  Techniques,
  MediaQueries,
  Flexbox,
  Grid,
  HelpfulThings
};
